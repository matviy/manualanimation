package com.way.mat.manualanimation;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

import com.thefinestartist.utils.ui.DisplayUtil;

public class MainActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener {

    ObjectAnimator inAnimator;
    ObjectAnimator inAnimator1;
    ObjectAnimator inAnimator2;
    ObjectAnimator inAnimator3;
    ObjectAnimator inAnimator4;
    ObjectAnimator inAnimator5;
    AnimatorSet setA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView img = (ImageView) findViewById(R.id.image);
        SeekBar seek = (SeekBar) findViewById(R.id.seek);

        inAnimator = ObjectAnimator.ofFloat(img, "rotationX", 0, -90).setDuration(1000);
        inAnimator1 = ObjectAnimator.ofFloat(img, "rotationY", 0, -720).setDuration(1000);
        inAnimator2 = ObjectAnimator.ofFloat(img, "y", 0, DisplayUtil.getHeight() * 0.5f).setDuration(1000);
        inAnimator3 = ObjectAnimator.ofFloat(img, "x", 0, DisplayUtil.getWidth() * 0.5f).setDuration(1000);
        inAnimator4 = ObjectAnimator.ofFloat(img, "scaleX", 1f, 0.3f).setDuration(500);
        inAnimator5 = ObjectAnimator.ofFloat(img, "scaleY", 1f, 1f).setDuration(1000);


        setA = new AnimatorSet();

        setA.playTogether(inAnimator, inAnimator1, inAnimator2, inAnimator3, inAnimator4, inAnimator5);

        seek.setOnSeekBarChangeListener(this);

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
//        inAnimator.setCurrentPlayTime(i);
//        inAnimator1.setCurrentPlayTime(i);
        for (Animator a : setA.getChildAnimations()) {
            ((ObjectAnimator) a).setCurrentPlayTime(i);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
//        inAnimator.start();
        Toast.makeText(this, "test", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
