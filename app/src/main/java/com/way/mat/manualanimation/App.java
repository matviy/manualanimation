package com.way.mat.manualanimation;

import android.app.Application;

import com.thefinestartist.Base;

/**
 * Created by matviy on 13.10.16.
 */

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Base.initialize(this);
    }
}
